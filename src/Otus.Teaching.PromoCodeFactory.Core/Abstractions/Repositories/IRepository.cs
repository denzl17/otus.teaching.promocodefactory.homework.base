﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task<IEnumerable<T>> FilterAsync(Expression<Func<T, bool>> predicate);

        Task<bool> CreateAsync(T ent);

        Task<bool> DeleteAsync(T ent);

        Task<bool> UpdateAsync(T ent);

        void SaveChanges();
    }
}