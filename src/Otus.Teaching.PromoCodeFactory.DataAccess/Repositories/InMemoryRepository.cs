﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id) 
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<IEnumerable<T>> FilterAsync(Expression<Func<T, bool>> predicate)
        {
            return Task.FromResult(Data.Where(predicate.Compile()));
        }

        public Task<bool> CreateAsync(T ent)
        {
            if (ent is null)
                return Task.FromResult(false);

            if (Data.Contains(ent))
                return Task.FromResult(true);

            // generate new
            ent.Id = new Guid();
            Data = Data.Append(ent);

            return Task.FromResult(true);
        }

        public Task<bool> DeleteAsync(T ent)
        {
            var item = Data.FirstOrDefault(x => x.Id == ent.Id);

            if (item is null)
                return Task.FromResult(false);

            // delete
            Data = Data.Where(x => x.Id != ent.Id); 

            return Task.FromResult(true);
        }

        public Task<bool> UpdateAsync(T ent)
        {
            if (ent is null)
                return Task.FromResult(false);

            if (Data.Contains(ent))
                return Task.FromResult(true);

            // check
            var item = Data.FirstOrDefault(x => x.Id == ent.Id);

            if (item is null)
                return Task.FromResult(false);

            // delete and add
            Data = Data
                .Where(x => x.Id != ent.Id)
                .Append(ent);

            return Task.FromResult(true);
        }

        public void SaveChanges() { }
    }
}